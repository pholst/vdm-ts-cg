package org.overture.codegen.vdm2js;

import java.util.LinkedList;
import java.util.List;

import org.overture.codegen.ir.SExpIR;
import org.overture.codegen.ir.SStmIR;
import org.overture.codegen.ir.STypeIR;
import org.overture.codegen.ir.analysis.AnalysisException;
import org.overture.codegen.ir.analysis.DepthFirstAnalysisAdaptor;
import org.overture.codegen.ir.analysis.intf.IAnalysis;
import org.overture.codegen.ir.declarations.AFormalParamLocalParamIR;
import org.overture.codegen.ir.declarations.AMethodDeclIR;
import org.overture.codegen.ir.declarations.AVarDeclIR;
import org.overture.codegen.ir.expressions.AApplyExpIR;
import org.overture.codegen.ir.expressions.ACompSetExpIR;
import org.overture.codegen.ir.expressions.ALambdaExpIR;
import org.overture.codegen.ir.expressions.ALetDefExpIR;
import org.overture.codegen.ir.expressions.AMapIterationBinaryExpIR;
import org.overture.codegen.ir.expressions.APowerNumericBinaryExpIR;
import org.overture.codegen.ir.statements.ABlockStmIR;
import org.overture.codegen.ir.statements.AReturnStmIR;
import org.overture.codegen.ir.types.AMapMapTypeIR;
import org.overture.codegen.ir.types.AMethodTypeIR;
import org.overture.codegen.trans.assistants.TransAssistantIR;

public class JSTransSeries {
	
	private static JSFormat JSformat = null;
	private List<IAnalysis> series;
	TransAssistantIR transAssistant;
	
	@SuppressWarnings("static-access")
	public JSTransSeries(JSCodeGenBase jsCodeGen) {
		series = new LinkedList<>();
		transAssistant = jsCodeGen.getTransAssistant();
		JSformat = new JSFormat(jsCodeGen.JS_TEMPLATES_ROOT_FOLDER, jsCodeGen.getInfo());
		
		//series.add(new PrintVisitor());

		
		series.add(new DepthFirstAnalysisAdaptor() 
		{
			@Override
			public void caseAMethodDeclIR(AMethodDeclIR node) throws AnalysisException {
				SStmIR body = node.getBody(); 
				AMethodTypeIR methodType = node.getMethodType();
				STypeIR resultType = methodType.getResult();
				
				handleReturnStm(body, resultType);
			}
		});
		
		series.add(new DepthFirstAnalysisAdaptor() 
		{
			@Override
			public void caseAPowerNumericBinaryExpIR(APowerNumericBinaryExpIR node) throws AnalysisException {
				if(node.getType() instanceof AMapMapTypeIR){
					AMapIterationBinaryExpIR mapIter = new AMapIterationBinaryExpIR();
					mapIter.setLeft(node.getLeft());
					mapIter.setRight(node.getRight());
					mapIter.setType(node.getType());
					mapIter.setMetaData(node.getMetaData());
					mapIter.setSourceNode(node.getSourceNode());
					mapIter.setTag(node.getTag());
					
					transAssistant.replaceNodeWith(node, mapIter);
				}
			}
		});
		
		series.add(new DepthFirstAnalysisAdaptor() {
			@Override
			public void caseALetDefExpIR(ALetDefExpIR node) throws AnalysisException {
				
				LinkedList<AFormalParamLocalParamIR> params = new LinkedList<>();
				LinkedList<STypeIR> paramsType = new LinkedList<>();
				LinkedList<SExpIR> args = new LinkedList<SExpIR>();
				for (AVarDeclIR varDecl : node.getLocalDefs()) {
					AFormalParamLocalParamIR formalParam = new AFormalParamLocalParamIR();
					formalParam.setType(varDecl.getType());
					formalParam.setPattern(varDecl.getPattern());

					args.add(varDecl.getExp());
					params.add(formalParam);
					paramsType.add(varDecl.getType());
				}

				AMethodTypeIR methodType = new AMethodTypeIR();
				methodType.setResult(node.getType().clone());

				ALambdaExpIR lambdaExp = new ALambdaExpIR();
				lambdaExp.setParams(params);
				lambdaExp.setExp(node.getExp().clone());

				AApplyExpIR applyExp = new AApplyExpIR();
				applyExp.setRoot(lambdaExp);
				applyExp.setArgs(args);
				applyExp.setType(node.getType().clone());
				
				transAssistant.replaceNodeWithRecursively(node, applyExp, this);
			}
		});
	
		
	}
	
	public List<IAnalysis> getSeries() {
		return series;
	}
	
	
	private void handleReturnStm(SStmIR stm, STypeIR resultType) throws AnalysisException {
		if(stm instanceof AReturnStmIR) {
			if(((AReturnStmIR) stm).getExp() != null && 
					!JSformat.format(resultType).equals(JSformat.format(((AReturnStmIR) stm).getExp().getType()))) {
				((AReturnStmIR) stm).setCast(resultType.clone());
			} else {
				((AReturnStmIR) stm).setCast(null);
			}
		} else if(stm instanceof ABlockStmIR) {
			LinkedList<SStmIR> stmList = ((ABlockStmIR) stm).getStatements();
			for(SStmIR s : stmList) {
				handleReturnStm(s, resultType);
			}
		}
	}
	
}
