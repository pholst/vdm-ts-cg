package org.overture.codegen.vdm2js;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.overture.ast.analysis.AnalysisException;
import org.overture.ast.expressions.PExp;
import org.overture.codegen.ir.CodeGenBase;
import org.overture.codegen.ir.IRStatus;
import org.overture.codegen.ir.PIR;
import org.overture.codegen.ir.SExpIR;
import org.overture.codegen.ir.analysis.intf.IAnalysis;
import org.overture.codegen.ir.declarations.ADefaultClassDeclIR;
import org.overture.codegen.logging.Logger;
import org.overture.codegen.merging.MergeVisitor;
import org.overture.codegen.trans.DivideTrans;
import org.overture.codegen.utils.Generated;
import org.overture.codegen.utils.GeneratedData;
import org.overture.codegen.utils.GeneratedModule;

public class JSCodeGenBase extends CodeGenBase {
	
	public static final String JS_TEMPLATES_ROOT_FOLDER = "JSTemplates";

	private JSTransSeries transSeries;
	private JSFormat jsFormat;
	
	public JSCodeGenBase() {
		super();
		jsFormat = new JSFormat(JS_TEMPLATES_ROOT_FOLDER, this.getInfo());
		transSeries = new JSTransSeries(this);
	}

	@Override
	protected GeneratedData genVdmToTargetLang(List<IRStatus<PIR>> statuses) throws AnalysisException {
		
		
		List<IRStatus<ADefaultClassDeclIR>> classes = IRStatus.extract(statuses, ADefaultClassDeclIR.class);
		
		List<GeneratedModule> genModules = new LinkedList<>();
		
		// Apply transformations 
		for(IAnalysis t : transSeries.getSeries())
		{
			for(IRStatus<ADefaultClassDeclIR> s : classes)
			{
				try {
					generator.applyPartialTransformation(s, t);
				} catch (org.overture.codegen.ir.analysis.AnalysisException e) {
					e.printStackTrace();
				}
			}
		}
		
		// Laver kode
		for(IRStatus<ADefaultClassDeclIR> s : classes)
		{
			try {
				genModules.add(genIrModule(jsFormat.getMergeVisitor(), s));
			} catch (org.overture.codegen.ir.analysis.AnalysisException e) {
				e.printStackTrace();
			}
		}
		
		GeneratedData data = new GeneratedData();
		data.setClasses(genModules);
		
		return data;
	}

	public Generated generateJSFromVdmExp(PExp exp) throws AnalysisException, org.overture.codegen.ir.analysis.AnalysisException
	{
		// There is no name validation here.
		IRStatus<SExpIR> expStatus = generator.generateFrom(exp);
		
		generator.applyPartialTransformation(expStatus, new DivideTrans(getInfo()));

		MergeVisitor mergeVisitor = jsFormat.getMergeVisitor();

		try
		{
			return genIrExp(expStatus, mergeVisitor);

		} catch (org.overture.codegen.ir.analysis.AnalysisException e)
		{
			Logger.getLog().printErrorln("Could not generate expression: "
					+ exp);
			e.printStackTrace();
			return null;
		}
	}

	
	
	public String getQuoteTypes(){
		StringBuilder sb = new StringBuilder();
		Iterator<String> quoteIter = jsFormat.quotes.iterator();
		sb.append("namespace Quotes {\n");
		while(quoteIter.hasNext()) {
			String name = quoteIter.next();
			sb.append("export class "+name+" extends _.Quote { \n");
			sb.append("private static instance = new "+name+"();\n");
			sb.append("constructor() {\n");
			sb.append("super();\n");
			sb.append("if ("+name+".instance) {\n");
			sb.append("throw Error(\"Cannot construct instance. Use getInstance()\");\n");
			sb.append("}\n");
			sb.append(name+".instance = this;\n");
			sb.append("}\n");
			sb.append("public static getInstance() {\n");
			sb.append("return this.instance;\n");
			sb.append("}\n");
			sb.append("public toString() {\n");
			sb.append("return '<"+name+">';\n");
			sb.append("}\n");
			
			sb.append("public equals(quote: _.Quote) {\n");
			sb.append("return quote instanceof "+name+";\n");
			sb.append("}\n");
			
			sb.append("}\n");
		}
		sb.append("}\n");
		return sb.toString();
	}

}
