package org.overture.codegen.vdm2js;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.overture.ast.analysis.AnalysisException;
import org.overture.ast.definitions.SClassDefinition;
import org.overture.ast.lex.Dialect;
import org.overture.codegen.ir.CodeGenBase;
import org.overture.codegen.ir.IRSettings;
import org.overture.codegen.logging.Logger;
import org.overture.codegen.utils.GeneralCodeGenUtils;
import org.overture.codegen.utils.GeneratedData;
import org.overture.codegen.utils.GeneratedModule;
import org.overture.config.Settings;
import org.overture.typechecker.util.TypeCheckerUtil;
import org.overture.typechecker.util.TypeCheckerUtil.TypeCheckResult;

public class JSCodeGen extends JSCodeGenBase {

	public static final String JS_TEMPLATES_ROOT_FOLDER = "JSTemplates";
	
	static JSCodeGenBase jsCodeGenBase;
	
	public JSCodeGen() {
		
		Settings.dialect = Dialect.VDM_PP;
		
		IRSettings irSettings = new IRSettings();
		irSettings.setCharSeqAsString(false);
		irSettings.setGeneratePreConds(false);
		irSettings.setGeneratePreCondChecks(false);
		irSettings.setGeneratePostConds(false);
		irSettings.setGeneratePostCondChecks(false);

		jsCodeGenBase = new JSCodeGenBase();
		jsCodeGenBase.setSettings(irSettings);
	}
	
	public String generateJS(File file) {
		return generateJS(file.getAbsolutePath());
	}

	public String generateJS(String path) {
		
		List<File> files = loadFiles(path);
		
		TypeCheckResult<List<SClassDefinition>> tcResult = TypeCheckerUtil.typeCheckPp(files);

		if (!tcResult.parserResult.errors.isEmpty()) {
			Logger.getLog().printErrorln("Parser error");
			return null;
		}
		
		

		if (!tcResult.errors.isEmpty()) {
			Logger.getLog().printErrorln("Type check errors");
			System.out.println(tcResult.errors);
			return null;
		}

		/*
		List<SClassDefinition> f = tcResult.result;
		for (SClassDefinition s : f) {
			try {
				s.apply(new AstPrintVisitor());
			} catch (AnalysisException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		System.out.println();
		*/
		
		try {
			GeneratedData data = jsCodeGenBase.generate(CodeGenBase.getNodes(tcResult.result));
			return processData(false, "output", data, false);
		} catch (AnalysisException ae) {
			Logger.getLog().println("Could not code generate model: "
					+ ae.getMessage());
		}
		
		return null;
	}
	
	private static String processData (boolean printCode, String outputDir, GeneratedData data, boolean separateTestCode) {
		
		StringBuilder writer = new StringBuilder();
		List<GeneratedModule> generatedClasses = data.getClasses();
		
		writer.append(jsCodeGenBase.getQuoteTypes()+"\n");

		for (GeneratedModule generatedClass : generatedClasses) {
			if (generatedClass.hasMergeErrors())
			{
				Logger.getLog().println(String.format("Class %s could not be merged. Following merge errors were found:", generatedClass.getName()));
				GeneralCodeGenUtils.printMergeErrors(generatedClass.getMergeErrors());
			} else if (!generatedClass.canBeGenerated())
			{
				Logger.getLog().println("Could not generate class: "
						+ generatedClass.getName() + "\n");

				if (generatedClass.hasUnsupportedIrNodes())
				{
					Logger.getLog().println("Following VDM constructs are not supported by the code generator:");
					GeneralCodeGenUtils.printUnsupportedIrNodes(generatedClass.getUnsupportedInIr());
				}

				if (generatedClass.hasUnsupportedTargLangNodes())
				{
					Logger.getLog().println("Following constructs are not supported by the code generator:");
					GeneralCodeGenUtils.printUnsupportedNodes(generatedClass.getUnsupportedInTargLang());
					Logger.getLog().println("Additional info: " + generatedClass.getUnsupportedInTargLang().toString());
				}

			} else {
				
				
				
				writer.append(generatedClass.getContent());
			}
		}
		// TODO : Move to test class?
		return writer.append("").toString();
	}
	

	private static List<File> loadFiles(String pathToVdmFiles) {
		try {
			return Files.walk(Paths.get(pathToVdmFiles))
					.filter(Files::isRegularFile)
					.map(Path::toFile)
					.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	

}
