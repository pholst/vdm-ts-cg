package org.overture.codegen.vdm2js;

import org.overture.codegen.ir.analysis.AnalysisAdaptor;
import org.overture.codegen.ir.analysis.AnalysisException;
import org.overture.codegen.ir.analysis.DepthFirstAnalysisAdaptor;
import org.overture.codegen.ir.declarations.ADefaultClassDeclIR;
import org.overture.codegen.ir.declarations.ATypeDeclIR;
import org.overture.codegen.ir.name.ATypeNameIR;
import org.overture.codegen.ir.types.AIntNumericBasicTypeIR;

public class PrintVisitor extends DepthFirstAnalysisAdaptor {

	@Override
	public void caseADefaultClassDeclIR(ADefaultClassDeclIR node) throws AnalysisException {
		System.out.println("Class");
		super.caseADefaultClassDeclIR(node);
	}


	@Override
	public void caseATypeNameIR(ATypeNameIR node) throws AnalysisException {
		System.out.println("TypeNameIR: "+ node.getName());
		super.caseATypeNameIR(node);
	}
	


	@Override
	public void caseATypeDeclIR(ATypeDeclIR node) throws AnalysisException {
		System.out.println("TypeDeclIR");
		super.caseATypeDeclIR(node);
	}

	@Override
	public void caseAIntNumericBasicTypeIR(AIntNumericBasicTypeIR node) throws AnalysisException {
		System.out.println("IntNumericBasicType");
		super.caseAIntNumericBasicTypeIR(node);
	}
	
	
	
}