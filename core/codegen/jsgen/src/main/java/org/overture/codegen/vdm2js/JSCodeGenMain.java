package org.overture.codegen.vdm2js;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.io.IOUtils;

public class JSCodeGenMain {
	
	public static void main(String[] args) throws FileNotFoundException {
		JSCodeGen jsCodeGen = new JSCodeGen();
		String codeGenContent = jsCodeGen.generateJS("vdmfiles");
		
		
		StringBuilder libContent = new StringBuilder();

		try (BufferedReader br = new BufferedReader(new FileReader("lib/src/Util.ts")))
		{

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				libContent.append(sCurrentLine + "\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 

		/*

		PrintWriter writer;
		try {

			writer = new PrintWriter("out.ts", "UTF-8");
			writer.println(libContent.toString());
			writer.println(codeGenContent);
			
			writer.println("console.log(new Test1().Run())");
			writer.close();

		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		*/
		
		System.out.println(libContent);
		System.out.println(codeGenContent);
		
	}

}
