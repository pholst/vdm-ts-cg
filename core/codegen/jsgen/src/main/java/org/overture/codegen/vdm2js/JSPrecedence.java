package org.overture.codegen.vdm2js;

import org.overture.codegen.ir.IROperatorInfo;
import org.overture.codegen.ir.IROperatorLookup;
import org.overture.codegen.ir.SExpIR;
import org.overture.codegen.ir.expressions.ADivideNumericBinaryExpIR;
import org.overture.codegen.ir.expressions.ASubtractNumericBinaryExpIR;

public class JSPrecedence {
	
	private IROperatorLookup opLookup;

	public JSPrecedence() {

		this.opLookup = new IROperatorLookup();
	}
	
	public boolean mustIsolate(SExpIR parentExp, SExpIR exp, boolean leftChild) {
		
		IROperatorInfo parentOpInfo = opLookup.find(parentExp.getClass());
		
		if (parentOpInfo == null) {
			return false;
		}

		IROperatorInfo expOpInfo = opLookup.find(exp.getClass());
		
		if (expOpInfo == null) {
			return false;
		}
		
		boolean case1 = !leftChild
				&& (parentExp instanceof ADivideNumericBinaryExpIR || parentExp instanceof ASubtractNumericBinaryExpIR)
				&& parentOpInfo.getPrecedence() >= expOpInfo.getPrecedence();

		if (case1)
		{
			return true;
		}

		// Case 2: Protect against case like 1 / (1+2+3)
		boolean case2 = parentOpInfo.getPrecedence() > expOpInfo.getPrecedence();

		return case2;
		
		
	}

}
