package org.overture.codegen.vdm2js;

import java.io.StringWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.overture.codegen.ir.INode;
import org.overture.codegen.ir.IRInfo;
import org.overture.codegen.ir.SExpIR;
import org.overture.codegen.ir.SMultipleBindIR;
import org.overture.codegen.ir.SPatternIR;
import org.overture.codegen.ir.SStmIR;
import org.overture.codegen.ir.STypeIR;
import org.overture.codegen.ir.analysis.AnalysisException;
import org.overture.codegen.ir.declarations.AFieldDeclIR;
import org.overture.codegen.ir.declarations.AFormalParamLocalParamIR;
import org.overture.codegen.ir.declarations.ANamedTypeDeclIR;
import org.overture.codegen.ir.declarations.ARecordDeclIR;
import org.overture.codegen.ir.declarations.ATypeDeclIR;
import org.overture.codegen.ir.declarations.AVarDeclIR;
import org.overture.codegen.ir.declarations.SClassDeclIR;
import org.overture.codegen.ir.expressions.AApplyExpIR;
import org.overture.codegen.ir.expressions.ABoolLiteralExpIR;
import org.overture.codegen.ir.expressions.ACompSetExpIR;
import org.overture.codegen.ir.expressions.ADistIntersectUnaryExpIR;
import org.overture.codegen.ir.expressions.ADistUnionUnaryExpIR;
import org.overture.codegen.ir.expressions.AEnumMapExpIR;
import org.overture.codegen.ir.expressions.AEnumSeqExpIR;
import org.overture.codegen.ir.expressions.AEnumSetExpIR;
import org.overture.codegen.ir.expressions.AExists1QuantifierExpIR;
import org.overture.codegen.ir.expressions.ALambdaExpIR;
import org.overture.codegen.ir.expressions.ALetBeStExpIR;
import org.overture.codegen.ir.expressions.AMapIterationBinaryExpIR;
import org.overture.codegen.ir.expressions.AMapletExpIR;
import org.overture.codegen.ir.expressions.ANewExpIR;
import org.overture.codegen.ir.expressions.SBinaryExpBase;
import org.overture.codegen.ir.expressions.SQuantifierExpBase;
import org.overture.codegen.ir.name.ATypeNameIR;
import org.overture.codegen.ir.statements.ABlockStmIR;
import org.overture.codegen.ir.types.ACharBasicTypeIR;
import org.overture.codegen.ir.types.AClassTypeIR;
import org.overture.codegen.ir.types.AMapMapTypeIR;
import org.overture.codegen.ir.types.AMethodTypeIR;
import org.overture.codegen.ir.types.AQuoteTypeIR;
import org.overture.codegen.ir.types.ARecordTypeIR;
import org.overture.codegen.ir.types.ASeqSeqTypeIR;
import org.overture.codegen.ir.types.ASetSetTypeIR;
import org.overture.codegen.ir.types.AUnionTypeIR;
import org.overture.codegen.ir.utils.AHeaderLetBeStIR;
import org.overture.codegen.merging.MergeVisitor;
import org.overture.codegen.merging.TemplateCallable;
import org.overture.codegen.merging.TemplateManager;

public class JSFormat {

	private MergeVisitor mergeVisitor;
	
	public HashSet<String> quotes = new HashSet<String>();

	public JSFormat(String templateRoot, IRInfo info) {

		TemplateManager templateManager = new TemplateManager(templateRoot);
		TemplateCallable[] templateCallables = new TemplateCallable[] { 
				new TemplateCallable("JSFormat", this),
				new TemplateCallable("ValueSemantics", this)
			};

		mergeVisitor = new MergeVisitor(templateManager, templateCallables);

	}

	public String format(INode node) throws AnalysisException {
		StringWriter writer = new StringWriter();
		node.apply(mergeVisitor, writer);

		return writer.toString();
	}

	public String formatType(STypeIR node) throws AnalysisException {

		ANamedTypeDeclIR namedType = node.getNamedInvType();
		if (namedType != null) {
			return namedType.getName().toString();
		}
		return format(node);

	}

	public String format(SExpIR exp, boolean leftChild) throws AnalysisException {

		String formatedExp = format(exp);
		JSPrecedence precedence = new JSPrecedence();
		INode parent = exp.parent();

		if (!(parent instanceof SExpIR)) {
			return formatedExp;
		}

		boolean isolate = precedence.mustIsolate((SExpIR) parent, exp, leftChild);

		return isolate ? "(" + formatedExp + ")" : formatedExp;
	}
	

	public String formatSuperType(SClassDeclIR classDecl) {
		StringBuilder sb = new StringBuilder();
		sb.append(classDecl.getSuperNames().isEmpty() ? "" : "extends " + classDecl.getSuperNames().get(0));

		return sb.toString();
	}

	public String formatUnary(SExpIR exp) throws AnalysisException {
		return format(exp, false);
	}

	public String formatNotUnary(SExpIR exp) throws AnalysisException {
		String formattedExp = format(exp, false);

		boolean doNotWrap = exp instanceof ABoolLiteralExpIR
				|| formattedExp.startsWith("(") && formattedExp.endsWith(")");

		return doNotWrap ? "!" + formattedExp : "!(" + formattedExp + ")";
	}

	/* TODO: Maybe use this in AMethodDeclIR.vm */
	public String formatOperationBody(SStmIR body) throws AnalysisException {
		if (body == null) {
			return ";";
		}
		return "{" + format(body) + "}\n";
	}

	public String formatInitialExp(SExpIR exp) throws AnalysisException {
		return format(exp);
	}

	public MergeVisitor getMergeVisitor() {
		return mergeVisitor;
	}

	public String format(List<AFormalParamLocalParamIR> params) throws AnalysisException {
		StringBuilder sb = new StringBuilder();
		if (params.isEmpty()) {
			return "";
		}
		String delim = "";
		for (AFormalParamLocalParamIR param : params) {
			sb.append(delim);
			sb.append(format(param));
			delim = ", ";
		}
		return sb.toString();
	}

	public String typeDeclFormat(ATypeDeclIR type) throws AnalysisException {
		return format(type) + "\n";
	}
	

	/**
	 * Generate union type link to this in the constructor
	 * @param unionType
	 * @return CodeGen string
	 * @throws AnalysisException
	 */
	public String formatUnion(AUnionTypeIR union) throws AnalysisException {
		StringWriter writer = new StringWriter();
		// Construct union type
		LinkedList<STypeIR> types = union.getTypes();
		if (types.size() > 0) {
			writer.append(format(types.get(0)));
			for (int j = 1; j < types.size(); j++) {
				STypeIR type = types.get(j);
				writer.append("|" + format(type));
			}
		}
		// System.out.println(union.getSourceNode().getVdmNode());
		return writer.toString();
	}

	public String formatQuote(AQuoteTypeIR node) throws AnalysisException {
		quotes.add(node.getValue());
		return "Quotes." + node.getValue();
	}

	
	public String formatStaticFields(AFieldDeclIR node) throws AnalysisException {
		StringBuilder sb = new StringBuilder();
		if(node.getStatic()) {
			sb.append("export const ");
			sb.append(node.getName()+" ");
			sb.append(":"+ format(node.getType()) +" = ");
			if(!isRecordType(node.parent())){
				sb.append(formatInitialExp(node.getInitial())+";");
			}
		}
		return sb.toString();
	}
	
	
	public String formatRecordParamList(LinkedList<AFieldDeclIR> linkedList) throws AnalysisException {
		StringBuilder sb = new StringBuilder();
		if (!linkedList.isEmpty()) {
			String delim = "";
			for (AFieldDeclIR item : linkedList) {
				sb.append(delim);
				sb.append(item.getAccess());
				sb.append(" ");
				sb.append(item.getName());
				sb.append(": ");
				sb.append(format(item.getType()));
				delim = ", ";
			}
		}
		return sb.toString();
	}

	public String formatEqualityBinaryExps(SBinaryExpBase node) throws AnalysisException {
		// TODO : Handle comparison of different types
		STypeIR leftNodeType = node.getLeft().getType();

		if (leftNodeType instanceof ASetSetTypeIR || leftNodeType instanceof AMapMapTypeIR
				|| leftNodeType instanceof ARecordTypeIR
				|| leftNodeType instanceof AQuoteTypeIR) {
			return String.format("_.equals(%s, %s)", format(node.getLeft()), format(node.getRight()));
		} else if (leftNodeType instanceof ASeqSeqTypeIR) {
			return "Seq. not supported!";
		}
		return String.format("%s === %s", format(node.getLeft()), format(node.getRight()));
	}

	public String formatEnumSetExp(AEnumSetExpIR node) throws AnalysisException {
		StringBuilder writer = new StringBuilder();
		writer.append("new _.Set");
		if (node.getType() instanceof ASetSetTypeIR) {
			if (((ASetSetTypeIR) node.getType()).getSetOf() instanceof AUnionTypeIR) {
				writer.append("<" + format(((ASetSetTypeIR) node.getType()).getSetOf()) + ">");
			}
		}
		writer.append("(");

		LinkedList<SExpIR> members = node.getMembers();
		for (int i = 0; i < members.size(); i++) {
			if (i == members.size() - 1) {
				writer.append(format(members.get(i)));
			} else {
				writer.append(format(members.get(i)) + ",");
			}
		}
		writer.append(")");
		return writer.toString();
	}

	public String formatEnumMapExp(AEnumMapExpIR node) throws AnalysisException {
		StringBuilder writer = new StringBuilder();
		writer.append("new _.Map");
		if (node.getType() instanceof AMapMapTypeIR) {
			if (((AMapMapTypeIR) node.getType()).getFrom() instanceof AUnionTypeIR
					|| ((AMapMapTypeIR) node.getType()).getTo() instanceof AUnionTypeIR) {
				writer.append("<" + format(((AMapMapTypeIR) node.getType()).getFrom()) + ", "
						+ format(((AMapMapTypeIR) node.getType()).getTo()) + ">");
			}
		}
		writer.append("(");
		LinkedList<AMapletExpIR> members = node.getMembers();
		for (int i = 0; i < members.size(); i++) {
			if (i == members.size() - 1) {
				writer.append("\n\t" + format(members.get(i)));
			} else {
				writer.append("\n\t" + format(members.get(i)) + ",");
			}
		}
		writer.append(")");
		return writer.toString();
	}

	public String formatMapletExp(AMapletExpIR node) throws AnalysisException {
		StringBuilder writer = new StringBuilder();
		writer.append("new _.Maplet");
		/*
		 * explicitly annotate types in cases of union-type if(node.getType()
		 * instanceof AMapMapTypeIR) { if(node.getLeft().getType() instanceof
		 * AUnionTypeIR || node.getRight().getType() instanceof AUnionTypeIR) {
		 * writer.append("<"+ format(node.getLeft().getType()) +", " +
		 * format(node.getRight().getType()) +">"); } }
		 */

		writer.append("(");
		writer.append(format(node.getLeft()) + ", ");
		writer.append(format(node.getRight()));
		writer.append(")");
		return writer.toString();
	}
	
	public String formatExists1QuantifierExp(AExists1QuantifierExpIR node) throws AnalysisException {
		StringBuilder sb = new StringBuilder();

		SMultipleBindIR binding = node.getBindList().getFirst();
		
		sb.append(format(binding));
		sb.append(".filter(");
		sb.append(format(binding.getPatterns().getFirst()));
		sb.append(" => ");
		sb.append(format(node.getPredicate()));
		sb.append(").card() === 1");
		
		return sb.toString();

	}

	public String formatQuantifierExp(SQuantifierExpBase node, String fun) throws AnalysisException {
		
		StringBuilder sb = new StringBuilder();

		Iterator<SMultipleBindIR> bindings = node.getBindList().iterator();

		int i = 0;
		while (bindings.hasNext()) {
			SMultipleBindIR binding = bindings.next();

			Iterator<SPatternIR> patterns = binding.getPatterns().iterator();
			while (patterns.hasNext()) {
				SPatternIR pattern = patterns.next();
				
				sb.append(format(binding));
				sb.append("." + fun + "(");
				sb.append(format(pattern));
				sb.append(" => ");
				
				if (!bindings.hasNext() && !patterns.hasNext()) {
					sb.append(format(node.getPredicate()));
				}
				i++;
			}

		}
		for (int j=0; j<i; j++) { sb.append(")"); }


		return sb.toString();
		
	}
	
	public String formatLetBeStExp(ALetBeStExpIR node) throws AnalysisException {
		StringBuilder sb = new StringBuilder();
		
		sb.append("(() => {");
		sb.append("var res;");
		
		AHeaderLetBeStIR header = node.getHeader();
		SMultipleBindIR binding = header.getBinding();
		
		Iterator<SPatternIR> patterns = binding.getPatterns().iterator();
		while (patterns.hasNext()) {
			SPatternIR pattern = patterns.next();

			sb.append(format(binding));
			sb.append(".forEach(");
			sb.append(format(pattern));
			sb.append(" => ");
			
			if (!patterns.hasNext()) {
				sb.append("{");
				if (header.getSuchThat() == null) {
					sb.append("res = " + format(node.getValue()));
				} else {
					sb.append("if (");
					sb.append(format(header.getSuchThat()));
					sb.append(") {");
					sb.append("res = " + format(node.getValue()));
					sb.append("}");
				}
				sb.append("}");
			}
		}
		for (int i=0; i<binding.getPatterns().size(); i++) {
			sb.append(")");
		}
		sb.append(";");
		sb.append("if (!res) { throw 'Let be st found no applicable bindings' };");
		sb.append("return res");
		sb.append("})()");
		
		return sb.toString();
		
	}
	
	public String formatCompSetExp(ACompSetExpIR node) throws AnalysisException {
		
		StringBuilder sb = new StringBuilder();

		sb.append("(() => { var res = new ");
		sb.append(format(node.getType()));
		sb.append("();");
		
		Iterator<SMultipleBindIR> bindings = node.getBindings().iterator();
		int j = 0;
		while(bindings.hasNext()) {

			SMultipleBindIR binding = bindings.next();
			Iterator<SPatternIR> patterns = binding.getPatterns().iterator();
			while (patterns.hasNext()) {
				SPatternIR pattern = patterns.next();

				sb.append(format(binding));
				sb.append(".forEach(");
				sb.append(format(pattern));
				sb.append(" => ");
				
				if (!bindings.hasNext() && !patterns.hasNext()) {
					sb.append("{ if (");
					sb.append(format(node.getPredicate()));
					sb.append(") {");
					sb.append("res.add(");
					sb.append(format(node.getFirst()));
					sb.append("); } }");
				}
					
				++j;

			}
			
		}
		for (int i=0; i<j; i++) { sb.append(")"); }
		sb.append(";return res;");
		sb.append("})()");
		
		return sb.toString();
		
	}

	public String formatMapIteration(AMapIterationBinaryExpIR node) throws AnalysisException {
		String res = "";
		int n = Integer.parseInt(format(node.getRight()));
		String map = format(node.getLeft());

		if (n == 0) {
			res = map + ".identity()";
		} else if (n == 1) {
			res = map;
		} else {
			res = map;
			for (int i = 1; i < n; i++) {
				res += ".comp(" + map + ")";
			}
		}
		return res;
	}

	/*
	 * public String formatLetDefExp(ALetDefExpIR node) throws AnalysisException
	 * { String params = ""; String formalParams = ""; String body = ""; String
	 * returnType = format(node.getType()); LinkedList<AVarDeclIR> localDefs =
	 * node.getLocalDefs(); for (int i = 0; i < localDefs.size(); i++) { if(i ==
	 * localDefs.size()-1){ params += format(localDefs.get(i).getPattern());
	 * formalParams += format(localDefs.get(i).getExp()); } else { params +=
	 * format(localDefs.get(i).getPattern())+","; formalParams +=
	 * format(localDefs.get(i).getExp())+","; } } body += format(node.getExp());
	 * return "(("+params+"): "+returnType+" => "+body+")("+formalParams+")"; }
	 */

	public String formatDunionUnaryExp(ADistUnionUnaryExpIR node) throws AnalysisException {
		StringBuilder sb = new StringBuilder();
		
		
		SExpIR exp = node.getExp();
		if (exp instanceof AEnumSetExpIR) {
			LinkedList<SExpIR> members = ((AEnumSetExpIR) exp).getMembers();
			for (int i = 0; i < members.size(); i++) {
				if (i == members.size() - 1) {
					sb.append(
							format(members.get(i)) + String.join("", Collections.nCopies(i, String.valueOf(")"))));
				} else {
					sb.append(format(members.get(i)) + ".union(");
				}
			}
		}
		return sb.toString();
	}
	

	public String formatDinterUnaryExp(ADistIntersectUnaryExpIR node) throws AnalysisException {
		StringBuilder writer = new StringBuilder();
		SExpIR exp = node.getExp();
		if (exp instanceof AEnumSetExpIR) {
			LinkedList<SExpIR> members = ((AEnumSetExpIR) exp).getMembers();
			for (int i = 0; i < members.size(); i++) {
				if (i == members.size() - 1) {
					writer.append(
							format(members.get(i)) + String.join("", Collections.nCopies(i, String.valueOf(")"))));
				} else {
					writer.append(format(members.get(i)) + ".inter(");
				}
			}
		}
		return writer.toString();
	}

	public String formatCast(STypeIR node) throws AnalysisException {
		StringBuilder writer = new StringBuilder();
		if (node != null) {
			writer.append("as ");
			writer.append(format(node));
		}
		return writer.toString();
	}

	public String formatNewExp(ANewExpIR node) throws AnalysisException {
		String res = "";
		res += "new " + node.getName().getName() + "(";
		LinkedList<SExpIR> args = node.getArgs();
		for (int i = 0; i < args.size(); i++) {
			res += format(args.get(i)) + (i == args.size() - 1 ? "" : ",");
		}
		res += ")";
		return res;
	}

	public String formatBasicType(STypeIR node, String type) throws AnalysisException {
		String name = formatNamedTypeDecl(node.getNamedInvType());
		if (!name.isEmpty()) {
			return name;
		}
		return type;
	}

	public String formatUnionType(AUnionTypeIR node) throws AnalysisException {
		String name = formatNamedTypeDecl(node.getNamedInvType());
		if (!name.isEmpty()) {
			return name;
		}
		StringBuilder sb = new StringBuilder();
		String delim = "";
		for (STypeIR type : node.getTypes()) {
			sb.append(delim).append(format(type));
			delim = "|";
		}
		return sb.toString();
	}

	public String formatMapMapType(AMapMapTypeIR node) throws AnalysisException {
		String name = formatNamedTypeDecl(node.getNamedInvType());
		if (!name.isEmpty()) {
			return name;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("_.Map<");
		sb.append(format(node.getFrom())).append(",");
		sb.append(format(node.getTo()));
		sb.append(">");
		return sb.toString();
	}

	public String formatMethodType(AMethodTypeIR node) throws AnalysisException {
		String name = formatNamedTypeDecl(node.getNamedInvType());
		if (!name.isEmpty()) {
			return name;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("(");
		Iterator<STypeIR> iter = node.getParams().iterator();
		String delim = "";
		int i = 1;
		while (iter.hasNext()) {
			sb.append(delim).append("x").append(i).append(": ");
			sb.append(format(iter.next()));
			delim = ", ";
			i++;
		}
		sb.append(") => ");
		sb.append(format(node.getResult()));
		return sb.toString();
	}

	private String formatNamedTypeDecl(ANamedTypeDeclIR node) throws AnalysisException {
		return node != null ? formatTypeName(node, node.getName()) : "";
	}

	public String formatSetSetType(ASetSetTypeIR node) throws AnalysisException {
		String name = formatNamedTypeDecl(node.getNamedInvType());
		if (!name.isEmpty()) {
			return name;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("_.Set<");
		sb.append(format(node.getSetOf()));
		sb.append(">");
		return sb.toString();
	}

	public String formatName(INode node) throws AnalysisException {

		if (node instanceof ANewExpIR) {
			ANewExpIR newExp = (ANewExpIR) node;

			return formatTypeName(node, newExp.getName());
		} else if (node instanceof ARecordTypeIR) {
			ARecordTypeIR record = (ARecordTypeIR) node;
			ATypeNameIR typeName = record.getName();

			return formatTypeName(node, typeName);
		}

		throw new AnalysisException("Unexpected node in formatName: " + node.getClass().getName());
	}

	public String formatTypeName(INode node, ATypeNameIR typeName) {
		// Type names are also used for quotes, which do not have a defining
		// class.
		String typeNameStr = "";
		String defClass = typeName.getDefiningClass();

		SClassDeclIR classDecl = node.getAncestor(SClassDeclIR.class);
		ATypeDeclIR typeDecl = node.getAncestor(ATypeDeclIR.class);

		if (defClass != null) {
			if (!classDecl.getName().equals(defClass) || typeDecl == null) {
				typeNameStr += defClass + ".";
			}
		}
		typeNameStr += typeName.getName();

		return typeNameStr;

	}

	public String format(ALambdaExpIR node) throws AnalysisException {
		StringBuilder params = new StringBuilder();
		Iterator<AFormalParamLocalParamIR> iter = node.getParams().iterator();
		String delim = "";
		// System.out.println(format(node.getType()));
		while (iter.hasNext()) {
			params.append(delim).append(format(iter.next()));
			delim = ", ";
		}
		return "(" + params.toString() + ") => " + format(node.getExp());
	}

	public String format(AApplyExpIR node) throws AnalysisException {
		String args = "(" + formatArgs(node.getArgs()) + ")";
		SExpIR root = node.getRoot();
		if (root instanceof ALambdaExpIR) {
			return "(" + format(root) + ")" + args;
		}
		if (root.getType() instanceof AMapMapTypeIR) {
			return format(root) + ".apply" + args + "";
		}
		return format(root) + args;
	}

	public String formatEnumSeqExp(AEnumSeqExpIR node) throws AnalysisException {
		StringBuilder writer = new StringBuilder();
		LinkedList<SExpIR> members = node.getMembers();

		if (((ASeqSeqTypeIR) node.getType()).getSeqOf() instanceof ACharBasicTypeIR) {
			writer.append("\"");
			for (int i = 0; i < members.size(); i++) {
				writer.append(members.get(i));
			}
			writer.append("\"");
		} else if (node.getType() instanceof ASeqSeqTypeIR) {
			if (((ASeqSeqTypeIR) node.getType()).getSeqOf() instanceof AUnionTypeIR) {
				writer.append("<" + format(((ASetSetTypeIR) node.getType()).getSetOf()) + ">");
			}
			writer.append("(");
			for (int i = 0; i < members.size(); i++) {
				writer.append(format(members.get(i)) + (i == members.size() - 1 ? "" : ","));
			}
			writer.append(")");
		}
		return writer.toString();
	}

	public String formatSeqType(ASeqSeqTypeIR node) throws AnalysisException {
		if (node.getSeqOf() instanceof ACharBasicTypeIR) {
			return "string";
		}
		return "Sequence not supported!";
	}

	public String formatArgs(LinkedList<SExpIR> args) throws AnalysisException {
		StringBuilder sb = new StringBuilder();
		Iterator<SExpIR> iter = args.iterator();
		String delim = "";
		while (iter.hasNext()) {
			sb.append(delim).append(format(iter.next()));
			delim = ", ";
		}
		return sb.toString();
	}

	public boolean shouldClone(AVarDeclIR node) {
		if(node.getType() instanceof AClassTypeIR ||
		   node.getType() instanceof AQuoteTypeIR){
			return false;
		}
		return true;
	}
	
	public boolean hasSuperClass(SClassDeclIR classDecl) {
		return !classDecl.getSuperNames().isEmpty();
	}

	public boolean isRecordType(INode record) {
		return record != null && (record instanceof ARecordDeclIR);
	}
	
	public boolean isQuoteType(INode node) {
		return node != null && (node instanceof AQuoteTypeIR);
	}

	public boolean isMethodType(INode node) {
		return node != null && (node instanceof AMethodTypeIR);
	}

	public boolean isScoped(ABlockStmIR block) {
		return block != null && block.getScoped() != null && block.getScoped();
	}

	public boolean isNull(INode node) {
		return node == null;
	}

	
}