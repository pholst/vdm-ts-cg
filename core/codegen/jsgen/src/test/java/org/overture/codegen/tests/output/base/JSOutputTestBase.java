package org.overture.codegen.tests.output.base;

import java.util.List;

import org.overture.ast.analysis.AnalysisException;
import org.overture.ast.node.INode;
import org.overture.codegen.tests.output.util.OutputTestBase;
import org.overture.codegen.utils.GeneratedData;
import org.overture.codegen.vdm2js.JSCodeGenBase;

public abstract class JSOutputTestBase extends OutputTestBase {
	
	public JSOutputTestBase(String nameParameter, String inputParameter, String resultParameter) {
		super(nameParameter, inputParameter, resultParameter);
	}
	
	public JSCodeGenBase getJSGen()
	{
		JSCodeGenBase jsGen = new JSCodeGenBase();
		return jsGen;
	}

	@Override
	public GeneratedData genCode(List<INode> ast) throws AnalysisException {
		
		return getJSGen().generate(ast);
	}

}
