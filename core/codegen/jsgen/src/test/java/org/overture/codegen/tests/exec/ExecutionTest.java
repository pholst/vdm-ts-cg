package org.overture.codegen.tests.exec;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.overture.ast.lex.Dialect;
import org.overture.codegen.vdm2js.JSCodeGen;
import org.overture.interpreter.util.InterpreterUtil;

@RunWith(Parameterized.class)
public class ExecutionTest {

	private static JSCodeGen jsCodeGen = new JSCodeGen();
	
	@Parameters(name = "{index} : {0}")
	public static Collection<Object[]> data () {

		ArrayList<Object[]> list = new ArrayList<Object[]>();
		File[] files = new File("src/test/resources/exec").listFiles();
		for (File file : files) {
			list.add(new Object[] { file });
		}
		return list;
	}

	private String expected = "";
	private String actual = "";
	
	public ExecutionTest(File file) {
		
		try {
			expected = InterpreterUtil.interpret(Dialect.VDM_PP, "new Main().Run()", file).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		String js = jsCodeGen.generateJS(file.getAbsolutePath());

		ProcessBuilder pb = new ProcessBuilder("node", "-p", js);
		Map<String, String> env = pb.environment();
		env.put("PATH", env.get("PATH") + ";");
		StringBuilder writer = new StringBuilder();
		try { 
			Process proc = pb.start();
			BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = "";
			while ((line = br.readLine()) != null) {
				writer.append(line);
			}
			br.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		actual = writer.toString();
	}
	
	@Test
	public void test() {
		System.out.println(expected + "\t == \t" + actual);
		assertEquals(expected, actual);
	}


}



