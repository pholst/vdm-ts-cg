package org.overture.codegen.tests.output;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.overture.ast.analysis.AnalysisException;
import org.overture.ast.lex.Dialect;
import org.overture.codegen.logging.Logger;
import org.overture.codegen.tests.output.util.OutputTestUtil;
import org.overture.codegen.utils.GeneralUtils;
import org.overture.codegen.vdm2js.JSCodeGenBase;
import org.overture.codegen.vdm2js.JSCodeGenUtil;
import org.overture.config.Release;
import org.overture.config.Settings;
import org.overture.core.tests.ParamFineGrainTest;
import org.overture.core.tests.PathsProvider;

import com.google.gson.reflect.TypeToken;

@RunWith(Parameterized.class)
public class ExpOutputTest extends ParamFineGrainTest<String> {
	
	public static final String ROOT = "src" + File.separatorChar + "test"
			+ File.separatorChar + "resources" + File.separatorChar
			+ "expressions";

	public static final JSCodeGenBase jsCodeGen =  new JSCodeGenBase();

	public ExpOutputTest(String nameParameter, String testParameter, String resultParameter) {
		super(nameParameter, testParameter, resultParameter);
	}
	
	@BeforeClass
	public static void init()
	{
		Settings.dialect = Dialect.VDM_PP;
		Settings.release = Release.VDM_10;
	}

	@Parameters(name = "{index} : {0}")
	public static Collection<Object[]> testData() {
		return PathsProvider.computePaths(ROOT);
	}
			
	@Override
	public String processSource() {
		try
		{
			String fileContent = GeneralUtils.readFromFile(new File(modelPath));
			String generatedJs = JSCodeGenUtil.generateJSFromExp(fileContent, jsCodeGen, Settings.dialect).getContent().trim();
			String trimmed = GeneralUtils.cleanupWhiteSpaces(generatedJs);

			return trimmed;
		} catch (IOException | AnalysisException e)
		{
			e.printStackTrace();
			Assert.fail("Problems code generating expression to Java: "
					+ e.getMessage());
			return null;
		}
	}

	@Override
	public String deSerializeResult(String resultPath)
			throws FileNotFoundException, IOException
	{
		return OutputTestUtil.deSerialize(resultPath);
	}

	@Override
	public Type getResultType() {
		Type resultType = new TypeToken<String>()
		{
		}.getType();
		return resultType;
	}

	@Override
	protected String getUpdatePropertyString() {
		return OutputTestUtil.UPDATE_PROPERTY_PREFIX + "exp";
	}

	@Override
	public void compareResults(String actual, String expected) {
		OutputTestUtil.compare(expected, actual);
	}

}
