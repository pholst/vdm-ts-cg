
namespace _ {


    export class Record {

        public equals(r: Record): boolean {
            return Object.keys(this).every(e => _.equals(this[e], r[e]));
        }

        public toString(): string {
            var res = "mk_" + (<any>this.constructor).name + "(";
            res += Object.keys(this).map(e => <Object>this[e].toString()).join(", ");
            return res + ")";
        }

    }

    export class Token {

        constructor(public exp: Object) { }

        public equals(token: Token) {
            return _.equals(this.exp, token.exp);
        }

        public toString(): string {
            return "mk_token(" + this.exp.toString() + ")";
        }

    }


    export abstract class Quote {
        public abstract equals(quote: Quote) : boolean;
        public abstract toString() : string;
    }


    export function clone(o: Object): Object {
        if (typeof o == 'number') {
            return o;
        }
        if (o instanceof Set) {
            return null;
        }
        return;
    }


    export function equals(o1: Object, o2: Object): boolean {

        if (o1 instanceof Set && o2 instanceof Set) {
            return o1.equals(o2);
        }

        if (o1 instanceof Quote && o2 instanceof Quote) {
            return o1.equals(o2);
        }

        if (o1 instanceof Record && o2 instanceof Record) {
            return o1.equals(o2);
        }

        if (o1 instanceof Maplet && o2 instanceof Maplet) {
            return o1.equals(o2);
        }

        if (o1 instanceof Token && o2 instanceof Token) {
            return o1.equals(o2);
        }

        if (typeof o1 == 'number' && typeof o2 == 'number') {
            return o1 == o2;
        }

        if (typeof o1 == 'string' && typeof o2 == 'string') {
            return o1 == o2;
        }

        if (typeof o1 == 'boolean' && typeof o2 == 'boolean') {
            return o1 == o2;
        }
        return o1 === o2;

    }

    export function pretty(o: Object): string {

        if (o instanceof Set) {
            return "{" + Array.prototype.map.call(o, e => pretty(e)).join(', ') + "}";
        }

        if (o instanceof Map) {
            return "{" + Array.prototype.map.call(o.maplets, m => pretty(m)).join(', ') + "}";
        }

        if (o instanceof Record) {
            let res = "mk_" + (<any>o.constructor).name + "(";
            res += Object.keys(o).map(e => pretty(o[e])).join(", ");
            return res + ")";
        }

        if (o instanceof Maplet) {
            return pretty(o.key) + " |-> " + pretty(o.value);
        }

        if (o instanceof Quote) {
            return o.toString();
        }

        if (o instanceof Token) {
            return "mk_token(" + pretty(o.exp) + ")";
        }

        if (typeof o === 'number') {
            return o.toString();
        }

        if (typeof o === 'string') {
            return '"' + o.toString() + '"';
        }

        if (typeof o === 'boolean') {
            return o.toString();
        }

        return (<any>o.constructor).name + "{" + Object.keys(o).map(k => k + ":=" + pretty(o[k])) + "}";
    }

    export function range(a: number, b: number): Set<number> {
        var arr = new Array<number>();
        var i, a = Math.ceil(a), b = Math.floor(b);
        for (i = a; i <= b; i++) {
            arr.push(i);
        }
        return new Set<number>(arr);
    }

    export function isofbaseclass(name, exp): boolean {
        return Object.getPrototypeOf(name.prototype) === Object.prototype && exp instanceof name
    }

    export function samebaseclass(exp1, exp2): boolean {
        function getBaseCtor(proto) {
            if (Object.getPrototypeOf(proto) === Object.prototype) {
                return proto.constructor;
            } else {
                return getBaseCtor(Object.getPrototypeOf(proto));
            }
        }
        var baseCtor = getBaseCtor(Object.getPrototypeOf(exp1));
        return isofbaseclass(baseCtor, exp2);
    }

    export function sameclass(exp1, exp2): boolean {
        return Object.getPrototypeOf(exp1) === Object.getPrototypeOf(exp2);
    }

    export class Maplet<K, V> {

        constructor(public key: K, public value: V) { };

        public equals(m: Maplet<K, V>) {
            return _.equals(this.key, m.key) && _.equals(this.value, m.value);
        }

        public toString(): string {
            return this.key.toString() + " |-> " + this.value.toString();
        }

    }

    export class Map<K, V> {

        public maplets: Maplet<K, V>[] = new Array();

        constructor(...maplets: Maplet<K, V>[])
        constructor(maplets: Maplet<K, V>[])
        constructor(maplets) {
            if (arguments.length === 1) {
                maplets = Array.isArray(maplets) ? maplets : [maplets];
            } else {
                maplets = Array.prototype.slice.call(arguments);
            }
            this.putAll(maplets, false);
        }

        private put(maplet: Maplet<K, V>, overwrite?: boolean): void {
            if (!this.find((m, i, arr) => {
                if (_.equals(m.key, maplet.key)) {
                    if (overwrite) {
                        arr[i] = m;
                    } else {
                        if (!_.equals(m.value, maplet.value)) {
                            throw Error("Duplicate map keys have different values: " + m.key);
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            })) {
                this.maplets.push(maplet);
            }
        }

        public putAll(maplets: Maplet<K, V>[], overwrite?: boolean): Map<K, V> {
            maplets.forEach(m => this.put(m, overwrite));
            return this;
        }

        public dom(): Set<K> {
            return new Set<K>(this.maplets.map(m => m.key));
        }

        public rng(): Set<V> {
            return new Set<V>(this.maplets.map(m => m.value))
        }

        public munion<A, B>(map: Map<A, B>): Map<K|A, V|B> {
            var res = new Map<K|A, V|B>();
            return res.putAll(this.maplets, false).putAll(map.maplets, false);
        }

        public overwrite<A, B>(map: Map<A, B>): Map<K|A, V|B> {
            var res = new Map<K|A, V|B>();
            return res.putAll(this.maplets, true).putAll(map.maplets, true);
        }

        public domRestrictTo(set: Set<K>): Map<K, V> {
            return this.filter(m => set.inset(m.key));
        }

        public domRestrictBy(set: Set<K>): Map<K, V> {
            return this.filter(m => !set.inset(m.key));
        }

        public rngRestrictTo(set: Set<V>): Map<K, V> {
            return this.filter(m => set.inset(m.value));
        }

        public rngRestrictBy(set: Set<V>): Map<K, V> {
            return this.filter(m => !set.inset(m.value));
        }

        public apply(key: K): V {
            var m = this.find(m => _.equals(m.key, key));
            if (!m) {
                throw Error("No such key value in map: " + key.toString());
            }
            return m.value;
        }

        public comp<A>(map: Map<A, K>): Map<A, V> {
            if (!map.rng().subset(this.dom())) {
                throw Error("The RHS range is not a subset of the LHS domain");
            }
            var res = new Map<A, V>();
            map.forEach(m => res.put(new Maplet<A, V>(m.key, this.apply(m.value))));
            return res;
        }


        public inverse(): Map<V, K> {
            var res = new Map<V, K>();
            this.forEach(m => res.put(new Maplet<V, K>(m.value, m.key)));
            return res;
        }

        public size(): number {
            return this.maplets.length;
        }

        public equals(map: Map<K, V>): boolean {
            if (this.size() !== map.size()) {
                return false;
            }
            return this.every(m => map.contains(m));
        }

        public find(fn: (m: Maplet<K, V>, idx?: number, arr?: Maplet<K, V>[]) => boolean): Maplet<K, V> {
            var list = this.maplets;
            for (var i = 0; i < list.length; i++) {
                let value = list[i];
                if (fn(value, i, list)) {
                    return list[i];
                }
            }
        }

        public identity(): Map<K, K> {
            let res = new Map<K, K>();
            this.forEach(m => res.put(new Maplet<K, K>(m.key, m.key)));
            return res;
        }

        private contains(m: Maplet<K, V>): boolean {
            return !!this.find(ml => m.equals(ml));
        }

        public every(fn: (m: Maplet<K, V>) => boolean): boolean {
            return this.maplets.every(fn);
        }

        public filter(fn: (m: Maplet<K, V>) => boolean): Map<K, V> {
            return new Map<K, V>(this.maplets.filter(fn));
        }

        public forEach(fn: (m: Maplet<K, V>, idx?: number, arr?: Maplet<K, V>[]) => void): void {
            this.maplets.forEach(fn);
        }

        public map(fn: (m: Maplet<K, V>) => Maplet<K, V>): Map<K, V> {
            return new Map<K, V>(this.maplets.map(fn));
        }

        public toString(): string {
            return "{" + this.maplets.map(m => m.toString()).join(", ") + "}";
        }
    }

    export class Set<T> extends Array<T> {

        constructor(...elements: T[])
        constructor(elements: T[])
        constructor(elements) {
            super();
            if (arguments.length === 1) {
                elements = Array.isArray(elements) ? elements : [elements];
            } else {
                elements = Array.prototype.slice.call(arguments);
            }
            this.addAll(elements)
        }

        public add(a: T): void {
            if (!this.inset(a)) {
                super.push(a);
            }
        }

        private addAll(elms: T[]): Set<T> {
            elms.forEach(e => this.add(e));
            return this;
        }

        public inset(a: any): boolean {
            return super.some(e => _.equals(a, e));
        }

        public equals(set: Set<T>): boolean {
            if (this.card() != set.card()) {
                return false;
            }
            return this.every(e => set.inset(e));
        }

        public card(): number {
            return this.length;
        }

        public get(i: number): T {
            return this[i];
        }

        public union<U>(set: Set<U>): Set<T|U> {
            return new Set<T|U>().addAll(this).addAll(set);
        }

        public inter<U>(set: Set<U>): Set<T|U> {
            return this.filter(e => set.inset(e));
        }

        public diff<U>(set: Set<U>): Set<T|U> {
            return this.filter(e => !set.inset(e));
        }

        public subset<U>(set: Set<U>): boolean {
            return this.every(e => set.inset(e));
        }

        public psubset<U>(set: Set<U>): boolean {
            return this.subset(set) && this.card() < set.card();
        }

        public powerset(): Set<Set<T>> {
            if (this.card() == 0) {
                return new Set<Set<T>>(new Set<T>());
            } else {
                var elm = new Set<T>(this.get(0));
                var ps = this.diff(elm).powerset();
                return ps.union(ps.map(e => e.union(elm)));
            }
        }

        public filter(fn: (e: T) => boolean): Set<T> {
            return new Set<T>(super.filter(fn));
        }

        public map(fn: (e: T) => T): Set<T> {
            return new Set<T>(super.map(fn));
        }

        public toString(): string {
            return "{" + super.map(e => e.toString()).join(', ') + "}";
        }
    }
    
    
    // INITIALIZERS 

    export function set<T>(...elements: T[]) {
        return new Set<T>(elements);
    }

    export function maplet<K, V>(key: K, value: V) {
        return new Maplet<K, V>(key, value);
    }

    export function map<K, V>(...maplets: Maplet<K, V>[]) {
        return new Map<K, V>(maplets);
    }

    export function token(exp: Object) {
        return new Token(exp);
    }



}
