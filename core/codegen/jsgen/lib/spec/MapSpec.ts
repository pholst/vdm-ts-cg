import {Set, Map, Maplet, Util} from "../src/Util";
/// <reference path="jasmine.d.ts" />

describe("Map test suite!", function() {

    it("Should construct new Map with Maplet", function() {
        var map = new Map(new Maplet(true, 5));
        expect(map.size()).toBe(1);
    });

    it("Should throw exception when duplicate map keys have different values", function() {
        expect(function() {
            new Map(new Maplet(true, 2), new Maplet(true, 3))
        }).toThrowError(/duplicate map keys/i);
    });

    it("Should get domain of map", function() {
        var map = new Map(new Maplet(true, 1), new Maplet(false, 2));
        var res = new Set(true, false);
        expect(map.dom().equals(res)).toBe(true);
    });

    it("Should get range of map", function() {
        var map = new Map(
            new Maplet(1, true),
            new Maplet(2, false),
            new Maplet(3, true)
            );
        var set = new Set(true, false);
        expect(map.rng().equals(set)).toBe(true);
    });

    it("Should equal two identical maps", function() {
        var map1 = new Map(
            new Maplet(true, 4),
            new Maplet(false, 5)
            );

        var map2 = new Map(
            new Maplet(true, 4),
            new Maplet(false, 5)
            );

        expect(map1.equals(map2)).toBe(true);
    })

    it("Should not equal two different maps", function() {
        var map1 = new Map(
            new Maplet(true, 4),
            new Maplet(false, 5)
            );

        var map2 = new Map(
            new Maplet(true, 4),
            new Maplet(false, 6)
            );

        expect(map1.equals(map2)).toBe(false);
    });


    it("Should munion map", function() {
        var map1 = new Map(
            new Maplet("France", 9),
            new Maplet("Denmark", 4),
            new Maplet("SouthAfrica", 2),
            new Maplet("SaudiArabia", 1)
            );
        var map2 = new Map(
            new Maplet("England", 3)
            );
        var res = new Map(
            new Maplet("France", 9),
            new Maplet("Denmark", 4),
            new Maplet("SouthAfrica", 2),
            new Maplet("SaudiArabia", 1),
            new Maplet("England", 3)
            );
        expect(map1.munion(map2).equals(res)).toBe(true);
    });

    it("Should munion a map of union", function() {
        var a = new Map<number|boolean, boolean>(
            new Maplet(1, true),
            new Maplet(true, true),
            new Maplet(2, false)
            );
        var b = new Map<string, boolean|number>(
            new Maplet("e", true),
            new Maplet("d", 4),
            new Maplet("f", false)
            );
        expect(a.munion(b).size()).toBe(6);
    });

    it("Should overwrite maps without throw and exception", function() {
        var a = new Map<number, boolean>(
            new Maplet(1, true),
            new Maplet(2, true),
            new Maplet(3, true)
            );
        var b = new Map<number|boolean, number>(
            new Maplet(1, 9),
            new Maplet(2, 8),
            new Maplet(3, 7),
            new Maplet(true, 3)
            );
        var res = new Map<number|boolean, boolean|number>(
            new Maplet(1, true),
            new Maplet(2, true),
            new Maplet(3, true),
            new Maplet(true, 3)
            );
        expect(a.overwrite(b).equals(res)).toBe(true);
    });

    it("Should domain restrict to set", function() {
        var map = new Map(
            new Maplet(4, true),
            new Maplet(5, false),
            new Maplet(6, false)
            );
        var set = new Set(1, 2, 3, 4, 5);
        var res = new Map(
            new Maplet(4, true),
            new Maplet(5, false)
            );
        expect(map.domRestrictTo(set).equals(res)).toBe(true);
    });

    it("Should domain restrict by set", function() {
        var map = new Map(
            new Maplet(1, 2),
            new Maplet(2, 3),
            new Maplet(3, 4)
            );
        var set = new Set(2, 3);
        var res = new Map(
            new Maplet(1, 2)
            );
        expect(map.domRestrictBy(set).equals(res)).toBe(true);
    })

    it("Should range restrict to set", function() {
        var map = new Map(
            new Maplet(1, true),
            new Maplet(2, false),
            new Maplet(3, true),
            new Maplet(4, true)
            );
        var set = new Set(true);
        var res = new Map(
            new Maplet(1, true),
            new Maplet(3, true),
            new Maplet(4, true)
            );
        expect(map.rngRestrictTo(set).equals(res)).toBe(true);
    });

    it("Should range restrict by set", function() {
        var map = new Map(
            new Maplet(1, true),
            new Maplet(2, false),
            new Maplet(3, true),
            new Maplet(4, true)
            );
        var set = new Set(true);
        var res = new Map(
            new Maplet(2, false)
            );
        expect(map.rngRestrictBy(set).equals(res)).toBe(true);
    });

    it("Should get value for specified key in map", function() {
        var map = new Map(
            new Maplet(1, 10),
            new Maplet(2, 11),
            new Maplet(3, 12)
            );
        expect(map.apply(2)).toBe(11);
    });

    it("Should throw exception when attempting to get value for absent key in map", function() {
        var map = new Map(
            new Maplet(1, 10),
            new Maplet(2, 11),
            new Maplet(3, 12)
            );
        expect(function() { map.apply(5) }).toThrowError(/such key value/);
    });

    it("Should invert map that is 1-to-1 mapping", function() {
        var map = new Map(
            new Maplet(1, true),
            new Maplet(2, false)
            );
        var res = new Map(
            new Maplet(true, 1),
            new Maplet(false, 2)
            );
        expect(map.inverse().equals(res)).toBe(true);
    });

    it("Should throw exception when inverting map with no 1-to-1 mapping", function() {
        var map = new Map(
            new Maplet(1, true),
            new Maplet(2, false),
            new Maplet(3, true)
            );
        expect(function() { map.inverse() }).toThrowError();
    });

    it("Should fail map composition if RHS range is not subset of LHS domain", function() {
        var a = new Map(
            new Maplet(1, true),
            new Maplet(2, false)
            );
        var b = new Map(
            new Maplet("x", 1),
            new Maplet("y", 3)
            );
        expect(function() { a.comp(b) }).toThrowError(/RHS range is not a subset/i);
    });

    it("Should create map composition", function() {
        var a = new Map(
            new Maplet(1, true),
            new Maplet(2, false)
            );
        var b = new Map(
            new Maplet('x', 1),
            new Maplet('y', 2)
            );
        var res = new Map(
            new Maplet('x', true),
            new Maplet('y', false)
            );
        expect(a.comp(b).equals(res)).toBe(true);
    });

    it("Should create map composition with valid unions", function() {
        var a = new Map<number|boolean, boolean>(
            new Maplet(1, true),
            new Maplet(2, false),
            new Maplet(false, true)
            );
        var b = new Map<string, number|boolean>(
            new Maplet('x', 1),
            new Maplet('y', 2),
            new Maplet('z', false)
            );
        var res = new Map<string, boolean>(
            new Maplet('x', true),
            new Maplet('y', false),
            new Maplet('z', true)
            );
        expect(a.comp(b).equals(res)).toBe(true);
    });

    /*
        it("Should fail iteration when map range is not a subset of its domain", function() {
            var map = new Map(
                new Maplet(1, true),
                new Maplet(2, false)
                );
            expect(function() {
                Util.iter(map, 2)
            }).toThrowError(/map range is not a subset of its domain/i);
        });
        */


    it("Should return identity map when iterations is called with 0", function() {

        var a = new Map<number, number>(
            new Maplet(1, 2),
            new Maplet(2, 3),
            new Maplet(3, 5),
            new Maplet(4, 6),
            new Maplet(5, 4),
            new Maplet(6, 1)
            );

        var b = new Map<number|boolean, number>(
            new Maplet(1, 2),
            new Maplet(2, 1),
            new Maplet(false, 1));

        console.log(b.identity().toString());


        //console.log(map.iter(0).toString());

    });

});