var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var _;
(function (_) {
    var Record = (function () {
        function Record() {
        }
        Record.prototype.equals = function (r) {
            var _this = this;
            return Object.keys(this).every(function (e) { return _.equals(_this[e], r[e]); });
        };
        Record.prototype.toString = function () {
            var _this = this;
            var res = "mk_" + this.constructor.name + "(";
            res += Object.keys(this).map(function (e) { return _this[e].toString(); }).join(", ");
            return res + ")";
        };
        return Record;
    })();
    _.Record = Record;
    var Token = (function () {
        function Token(exp) {
            this.exp = exp;
        }
        Token.prototype.equals = function (token) {
            return _.equals(this.exp, token.exp);
        };
        Token.prototype.toString = function () {
            return "mk_token(" + this.exp.toString() + ")";
        };
        return Token;
    })();
    _.Token = Token;
    abstract;
    var Quote = (function () {
        function Quote() {
            this.abstract = equals(quote, Quote);
            this.abstract = toString();
        }
        return Quote;
    })();
    function clone(o) {
        if (typeof o == 'number') {
            return o;
        }
        if (o instanceof Set) {
            return null;
        }
        return;
    }
    _.clone = clone;
    function equals(o1, o2) {
        if (o1 instanceof Set && o2 instanceof Set) {
            return o1.equals(o2);
        }
        if (o1 instanceof Quote && o2 instanceof Quote) {
            return o1.equals(o2);
        }
        if (o1 instanceof Record && o2 instanceof Record) {
            return o1.equals(o2);
        }
        if (o1 instanceof Maplet && o2 instanceof Maplet) {
            return o1.equals(o2);
        }
        if (o1 instanceof Token && o2 instanceof Token) {
            return o1.equals(o2);
        }
        if (typeof o1 == 'number' && typeof o2 == 'number') {
            return o1 == o2;
        }
        if (typeof o1 == 'string' && typeof o2 == 'string') {
            return o1 == o2;
        }
        if (typeof o1 == 'boolean' && typeof o2 == 'boolean') {
            return o1 == o2;
        }
        return o1 === o2;
    }
    _.equals = equals;
    function pretty(o) {
        if (o instanceof Set) {
            return "{" + Array.prototype.map.call(o, function (e) { return pretty(e); }).join(', ') + "}";
        }
        if (o instanceof Map) {
            return "{" + Array.prototype.map.call(o.maplets, function (m) { return pretty(m); }).join(', ') + "}";
        }
        if (o instanceof Record) {
            var res = "mk_" + o.constructor.name + "(";
            res += Object.keys(o).map(function (e) { return pretty(o[e]); }).join(", ");
            return res + ")";
        }
        if (o instanceof Maplet) {
            return pretty(o.key) + " |-> " + pretty(o.value);
        }
        if (o instanceof Quote) {
            return o.toString();
        }
        if (o instanceof Token) {
            return "mk_token(" + pretty(o.exp) + ")";
        }
        if (typeof o === 'number') {
            return o.toString();
        }
        if (typeof o === 'string') {
            return '"' + o.toString() + '"';
        }
        if (typeof o === 'boolean') {
            return o.toString();
        }
        return o.constructor.name + "{" + Object.keys(o).map(function (k) { return k + ":=" + pretty(o[k]); }) + "}";
    }
    _.pretty = pretty;
    function range(a, b) {
        var arr = new Array();
        var i, a = Math.ceil(a), b = Math.floor(b);
        for (i = a; i <= b; i++) {
            arr.push(i);
        }
        return new Set(arr);
    }
    _.range = range;
    function isofbaseclass(name, exp) {
        return Object.getPrototypeOf(name.prototype) === Object.prototype && exp instanceof name;
    }
    _.isofbaseclass = isofbaseclass;
    function samebaseclass(exp1, exp2) {
        function getBaseCtor(proto) {
            if (Object.getPrototypeOf(proto) === Object.prototype) {
                return proto.constructor;
            }
            else {
                return getBaseCtor(Object.getPrototypeOf(proto));
            }
        }
        var baseCtor = getBaseCtor(Object.getPrototypeOf(exp1));
        return isofbaseclass(baseCtor, exp2);
    }
    _.samebaseclass = samebaseclass;
    function sameclass(exp1, exp2) {
        return Object.getPrototypeOf(exp1) === Object.getPrototypeOf(exp2);
    }
    _.sameclass = sameclass;
    var Maplet = (function () {
        function Maplet(key, value) {
            this.key = key;
            this.value = value;
        }
        ;
        Maplet.prototype.equals = function (m) {
            return _.equals(this.key, m.key) && _.equals(this.value, m.value);
        };
        Maplet.prototype.toString = function () {
            return this.key.toString() + " |-> " + this.value.toString();
        };
        return Maplet;
    })();
    _.Maplet = Maplet;
    var Map = (function () {
        function Map(maplets) {
            this.maplets = new Array();
            if (arguments.length === 1) {
                maplets = Array.isArray(maplets) ? maplets : [maplets];
            }
            else {
                maplets = Array.prototype.slice.call(arguments);
            }
            this.putAll(maplets, false);
        }
        Map.prototype.put = function (maplet, overwrite) {
            if (!this.find(function (m, i, arr) {
                if (_.equals(m.key, maplet.key)) {
                    if (overwrite) {
                        arr[i] = m;
                    }
                    else {
                        if (!_.equals(m.value, maplet.value)) {
                            throw Error("Duplicate map keys have different values: " + m.key);
                        }
                    }
                    return true;
                }
                else {
                    return false;
                }
            })) {
                this.maplets.push(maplet);
            }
        };
        Map.prototype.putAll = function (maplets, overwrite) {
            var _this = this;
            maplets.forEach(function (m) { return _this.put(m, overwrite); });
            return this;
        };
        Map.prototype.dom = function () {
            return new Set(this.maplets.map(function (m) { return m.key; }));
        };
        Map.prototype.rng = function () {
            return new Set(this.maplets.map(function (m) { return m.value; }));
        };
        Map.prototype.munion = function (map) {
            var res = new Map();
            return res.putAll(this.maplets, false).putAll(map.maplets, false);
        };
        Map.prototype.overwrite = function (map) {
            var res = new Map();
            return res.putAll(this.maplets, true).putAll(map.maplets, true);
        };
        Map.prototype.domRestrictTo = function (set) {
            return this.filter(function (m) { return set.inset(m.key); });
        };
        Map.prototype.domRestrictBy = function (set) {
            return this.filter(function (m) { return !set.inset(m.key); });
        };
        Map.prototype.rngRestrictTo = function (set) {
            return this.filter(function (m) { return set.inset(m.value); });
        };
        Map.prototype.rngRestrictBy = function (set) {
            return this.filter(function (m) { return !set.inset(m.value); });
        };
        Map.prototype.apply = function (key) {
            var m = this.find(function (m) { return _.equals(m.key, key); });
            if (!m) {
                throw Error("No such key value in map: " + key.toString());
            }
            return m.value;
        };
        Map.prototype.comp = function (map) {
            var _this = this;
            if (!map.rng().subset(this.dom())) {
                throw Error("The RHS range is not a subset of the LHS domain");
            }
            var res = new Map();
            map.forEach(function (m) { return res.put(new Maplet(m.key, _this.apply(m.value))); });
            return res;
        };
        Map.prototype.inverse = function () {
            var res = new Map();
            this.forEach(function (m) { return res.put(new Maplet(m.value, m.key)); });
            return res;
        };
        Map.prototype.size = function () {
            return this.maplets.length;
        };
        Map.prototype.equals = function (map) {
            if (this.size() !== map.size()) {
                return false;
            }
            return this.every(function (m) { return map.contains(m); });
        };
        Map.prototype.find = function (fn) {
            var list = this.maplets;
            for (var i = 0; i < list.length; i++) {
                var value = list[i];
                if (fn(value, i, list)) {
                    return list[i];
                }
            }
        };
        Map.prototype.identity = function () {
            var res = new Map();
            this.forEach(function (m) { return res.put(new Maplet(m.key, m.key)); });
            return res;
        };
        Map.prototype.contains = function (m) {
            return !!this.find(function (ml) { return m.equals(ml); });
        };
        Map.prototype.every = function (fn) {
            return this.maplets.every(fn);
        };
        Map.prototype.filter = function (fn) {
            return new Map(this.maplets.filter(fn));
        };
        Map.prototype.forEach = function (fn) {
            this.maplets.forEach(fn);
        };
        Map.prototype.map = function (fn) {
            return new Map(this.maplets.map(fn));
        };
        Map.prototype.toString = function () {
            return "{" + this.maplets.map(function (m) { return m.toString(); }).join(", ") + "}";
        };
        return Map;
    })();
    _.Map = Map;
    var Set = (function (_super) {
        __extends(Set, _super);
        function Set(elements) {
            _super.call(this);
            if (arguments.length === 1) {
                elements = Array.isArray(elements) ? elements : [elements];
            }
            else {
                elements = Array.prototype.slice.call(arguments);
            }
            this.addAll(elements);
        }
        Set.prototype.add = function (a) {
            if (!this.inset(a)) {
                _super.push.call(this, a);
            }
        };
        Set.prototype.addAll = function (elms) {
            var _this = this;
            elms.forEach(function (e) { return _this.add(e); });
            return this;
        };
        Set.prototype.inset = function (a) {
            return _super.some.call(this, function (e) { return _.equals(a, e); });
        };
        Set.prototype.equals = function (set) {
            if (this.card() != set.card()) {
                return false;
            }
            return this.every(function (e) { return set.inset(e); });
        };
        Set.prototype.card = function () {
            return this.length;
        };
        Set.prototype.get = function (i) {
            return this[i];
        };
        Set.prototype.union = function (set) {
            return new Set().addAll(this).addAll(set);
        };
        Set.prototype.inter = function (set) {
            return this.filter(function (e) { return set.inset(e); });
        };
        Set.prototype.diff = function (set) {
            return this.filter(function (e) { return !set.inset(e); });
        };
        Set.prototype.subset = function (set) {
            return this.every(function (e) { return set.inset(e); });
        };
        Set.prototype.psubset = function (set) {
            return this.subset(set) && this.card() < set.card();
        };
        Set.prototype.powerset = function () {
            if (this.card() == 0) {
                return new Set(new Set());
            }
            else {
                var elm = new Set(this.get(0));
                var ps = this.diff(elm).powerset();
                return ps.union(ps.map(function (e) { return e.union(elm); }));
            }
        };
        Set.prototype.filter = function (fn) {
            return new Set(_super.filter.call(this, fn));
        };
        Set.prototype.map = function (fn) {
            return new Set(_super.map.call(this, fn));
        };
        Set.prototype.toString = function () {
            return "{" + _super.map.call(this, function (e) { return e.toString(); }).join(', ') + "}";
        };
        return Set;
    })(Array);
    _.Set = Set;
    // INITIALIZERS 
    function set() {
        var elements = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            elements[_i - 0] = arguments[_i];
        }
        return new Set(elements);
    }
    _.set = set;
    function maplet(key, value) {
        return new Maplet(key, value);
    }
    _.maplet = maplet;
    function map() {
        var maplets = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            maplets[_i - 0] = arguments[_i];
        }
        return new Map(maplets);
    }
    _.map = map;
    function token(exp) {
        return new Token(exp);
    }
    _.token = token;
})(_ || (_ = {}));
