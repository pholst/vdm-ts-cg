var Util_1 = require("../src/Util");
/// <reference path="jasmine.d.ts" />
describe("Set test suite!", function () {
    it("Should add number to set", function () {
        var set = new Util_1.Set(3);
        expect(set.inset(3)).toBe(true);
        expect(set.card()).toBe(1);
    });
    it("Should not allow multiple values", function () {
        var set = Util_1.Util.set(3, 3);
        expect(set.card()).toBe(1);
    });
    it("Should add set of strings", function () {
        var set = new Util_1.Set("foo", "baz", "bar");
        var res = new Util_1.Set("foo", "bar", "baz");
        expect(set.equals(res)).toBe(true);
    });
    it("Should add set of booleans", function () {
        var set = new Util_1.Set(true, false, true, false);
        var res = new Util_1.Set(false, true);
        expect(set.equals(res)).toBe(true);
    });
    it("Should equal two empty sets", function () {
        var set1 = new Util_1.Set();
        var set2 = new Util_1.Set();
        expect(set1.equals(set1)).toBe(true);
    });
    it("Should add Set of numbers and contain afterwards", function () {
        var elm = new Util_1.Set(1, 2, 3);
        var set = new Util_1.Set(elm);
        expect(set.inset(new Util_1.Set(1, 2, 3))).toBe(true);
    });
    it("Should equal two identical sets of numbers in sequence", function () {
        var set1 = new Util_1.Set(1, 2, 3, 4);
        var set2 = new Util_1.Set(1, 2, 3, 4);
        expect(set1.equals(set2)).toBe(true);
    });
    it("Should equal two identical sets of numbers not in sequence", function () {
        var set1 = new Util_1.Set(4, 3, 2, 1);
        var set2 = new Util_1.Set(1, 2, 3, 4);
        expect(set1.equals(set2)).toBe(true);
    });
    it("Should not equal two non-identical sets of numbers", function () {
        var set1 = new Util_1.Set(1, 2, 3, 4);
        var set2 = new Util_1.Set(1, 2, 4, 5);
        expect(set1.equals(set2)).not.toBe(true);
    });
    it("Should equal two identical sets of sets of numbers in sequence", function () {
        var set1 = new Util_1.Set(new Util_1.Set(1, 2, 3), new Util_1.Set(4, 5, 6), new Util_1.Set(7, 8, 9));
        var set2 = new Util_1.Set(new Util_1.Set(1, 2, 3), new Util_1.Set(4, 5, 6), new Util_1.Set(7, 8, 9));
        expect(set1.equals(set2)).toBe(true);
    });
    it("Should equal two identical sets of sets of numbers not in sequence", function () {
        var set1 = new Util_1.Set(new Util_1.Set(3, 1, 2), new Util_1.Set(4, 5, 6), new Util_1.Set(7, 8, 9));
        var set2 = new Util_1.Set(new Util_1.Set(7, 9, 8), new Util_1.Set(1, 2, 3), new Util_1.Set(4, 5, 6));
        expect(set1.equals(set2)).toBe(true);
    });
    it("Should equal sets of unions", function () {
        var set = new Util_1.Set(new Util_1.Set(2, 9), new Util_1.Set(1, 2, 3), 3);
        var res = new Util_1.Set(new Util_1.Set(3, 1, 2), new Util_1.Set(9, 2), 3);
        expect(set.equals(res)).toBe(true);
    });
    // Clone test
    it("Should clone set without keeping references", function () {
        var elm = new Util_1.Set(1, 2, 3);
        var set = new Util_1.Set(elm);
        var cloned = set.clone();
        expect(cloned.get(0)).not.toBe(elm);
    });
    // Union test
    it("Should union set of numbers", function () {
        var set1 = new Util_1.Set(1, 2, 3);
        var set2 = new Util_1.Set(4, 5, 6);
        var result = new Util_1.Set(1, 2, 3, 4, 5, 6);
        expect(set1.union(set2).equals(result)).toBe(true);
    });
    it("Should union overlapping set of numbers", function () {
        var set1 = new Util_1.Set(9, 8, 1, 2, 3, 6);
        var set2 = new Util_1.Set(9, 1, 5, 2, 9, 5, 0);
        var result = new Util_1.Set(9, 1, 8, 2, 3, 5, 6, 0);
        expect(set2.union(set1).equals(result)).toBe(true);
    });
    it("Should union sets of different types", function () {
        var set1 = new Util_1.Set(1, 2, 3);
        var set2 = new Util_1.Set(true, false);
        var res = new Util_1.Set(1, 2, 3, true, false);
        expect(set1.union(set2).equals(res)).toBe(true);
    });
    it("Should union sets of unions", function () {
        var set1 = new Util_1.Set(1, 2, true);
        var set2 = new Util_1.Set("baz", "foo", new Util_1.Set(true, false, "foo", "bar"));
        var res = new Util_1.Set(1, 2, true, "baz", "foo", new Util_1.Set(true, false, "foo", "bar"));
        expect(set1.union(set2).equals(res)).toBe(true);
    });
    // Intersection test
    it("Should create intersect set of set of numbers", function () {
        var set1 = new Util_1.Set(1, 2, 3, 4);
        var set2 = new Util_1.Set(3, 4, 5, 6);
        var res = new Util_1.Set(3, 4);
        expect(set1.inter(set2).equals(res)).toBe(true);
    });
    it("Should create interesection of set of set of number", function () {
        var set1 = new Util_1.Set(new Util_1.Set(1, 2, 3), new Util_1.Set(2, 3, 4), new Util_1.Set(3, 4, 5));
        var set2 = new Util_1.Set(new Util_1.Set(1, 2, 3), new Util_1.Set(4, 9, 20), new Util_1.Set(4, 5, 3));
        var res = new Util_1.Set(new Util_1.Set(1, 2, 3), new Util_1.Set(3, 4, 5));
        expect(set1.inter(set2).equals(res)).toBe(true);
    });
    // Difference test
    it("Should create set of difference", function () {
        var set1 = new Util_1.Set(1, 2, 3, 4, 5);
        var set2 = new Util_1.Set(4, 5, 6, 7, 8);
        var res1 = new Util_1.Set(1, 2, 3);
        var res2 = new Util_1.Set(6, 7, 8);
        expect(set1.diff(set2).equals(res1)).toBe(true);
        expect(set2.diff(set1).equals(res2)).toBe(true);
    });
    it("Should create difference of set of set of numbers", function () {
        var set1 = new Util_1.Set(new Util_1.Set(1, 2, 3), new Util_1.Set(2, 3, 4), new Util_1.Set(3, 4, 5));
        var set2 = new Util_1.Set(new Util_1.Set(1, 2, 3), new Util_1.Set(4, 9, 20), new Util_1.Set(4, 5, 3));
        var res1 = new Util_1.Set(new Util_1.Set(2, 3, 4));
        var res2 = new Util_1.Set(new Util_1.Set(20, 9, 4));
        expect(set1.diff(set2).equals(res1)).toBe(true);
        expect(set2.diff(set1).equals(res2)).toBe(true);
    });
    // Subset tests
    it("Should be subset", function () {
        var set1 = new Util_1.Set(1, 2, 3, 4);
        var set2 = new Util_1.Set(2, 1, 5, 3, 4);
        var set3 = new Util_1.Set(4, 3, 1, 2);
        var set4 = new Util_1.Set();
        expect(set1.subset(set2)).toBe(true);
        expect(set1.subset(set3)).toBe(true);
        expect(set1.subset(set1)).toBe(true);
        expect(set4.subset(set1)).toBe(true);
    });
    it("Should be proper subset", function () {
        var set1 = new Util_1.Set(1, 2, 3, 4);
        var set2 = new Util_1.Set(1, 2, 3, 4);
        var set3 = new Util_1.Set(1, 2, 3, 4, 5);
        expect(set1.psubset(set2)).toBe(false);
        expect(set1.psubset(set3)).toBe(true);
    });
    // Powerset
    it("Should create powerset of empty set", function () {
        var res = new Util_1.Set(new Util_1.Set());
        expect(new Util_1.Set().powerset().equals(res)).toBe(true);
    });
    it("Should create powerset of set of numbers", function () {
        var set = new Util_1.Set(1, 2);
        var res = new Util_1.Set(new Util_1.Set(), new Util_1.Set(1), new Util_1.Set(2), new Util_1.Set(1, 2));
        expect(set.powerset().equals(res)).toBe(true);
    });
    // Range
    it("Should create set containing elements of specified range", function () {
        var set = Util_1.Util.range(1, 4);
        var res = new Util_1.Set(1, 2, 3, 4);
        expect(set.equals(res)).toBe(true);
    });
    it("Should create empty set with negative range", function () {
        var res = new Util_1.Set();
        expect(Util_1.Util.range(4, -3).equals(res)).toBe(true);
    });
    it("Should create correct set with floating point numbers specified", function () {
        var set = Util_1.Util.range(1.53, 3.2);
        var res = new Util_1.Set(2, 3);
        expect(set.equals(res)).toBe(true);
    });
});
